Interactive Brokers - Native IBAPI integration
==============================================

This integration with Interactive Brokers supports both Live Data feed and Live Trading. It is
meant to be a **drop-in replacement for the original backtrader ibpy implementation** (
`ibbroker <https://gitlab.com/algorithmic-trading-library/backtrader/-/blob/master/backtrader/brokers/ibbroker.py>`_,
`ibdata <https://gitlab.com/algorithmic-trading-library/backtrader/-/blob/master/backtrader/feeds/ibdata.py>`_ and
`ibstore <https://gitlab.com/algorithmic-trading-library/backtrader/-/blob/master/backtrader/stores/ibstore.py>`_).

Features
--------

* **Modifying Submitted/Open Orders**. see IBBroker.modify_order(). It includes the management of Advanced Order
  Rejection in cases where modifications happens when order is being filled/cancelled see IBBroker.push_ordererror() for
  errorCode [103, 104, 202].

Warnings
--------
1. **ONLY stock trading has been tested**. Options, Forex and other trading channels have not been tested at al.
2. Provided as-is: In spite of all attempts to test the maximum number of error conditions and situations, the code
could (like any other piece of software) contain bugs.

DISCLAIMER: RISKS ASSOCIATED WITH LIVE TRADING ALGORITHMS
=========================================================

The deployment of algorithms to live trading involves inherent risks that may result in the loss of capital.
Users are advised to carefully consider and understand the various risks associated with live trading, including but
not limited to strategy risks, portfolio risks, market risks, counterparty risks, operational risks, and error risks.

Strategy Risks:
The effectiveness of trading strategies is subject to market conditions, and unforeseen events may impact their
performance. Users should be aware that strategies may not always yield the desired results, and past performance
is not indicative of future success.

Portfolio Risks:
Diversification strategies may not fully protect against losses, and the performance of an entire portfolio may be
affected by the performance of individual assets within it.

Market Risks:
Fluctuations in market conditions and unexpected events can lead to significant financial losses. Users should be
prepared for market volatility and the potential impact on their trading strategies.

Counterparty Risks:
Transactions involve counterparties, and the failure of a counterparty can lead to financial losses. Users should
conduct due diligence on counterparties and be aware of the associated risks.

Operational Risks:
Operational risks pertain to business operations, including regulatory changes, business risks, and trading
infrastructure risks. Users are encouraged to stay informed about regulatory changes, utilize open-source trading
infrastructure maintained by experts, and consider co-located servers to mitigate hardware failures and internet
outages.

Error Risks:
Error risks are associated with errors in strategy logic or trading infrastructure. Bugs in trading algorithms and
underlying engines may lead to financial losses. Users should conduct thorough backtesting to identify coding errors
before deploying algorithms live and stay informed about relevant software issues.

While efforts can be made to mitigate these risks, it is important to note that some risks may be beyond the user's
control. Users are strongly advised to exercise caution, conduct comprehensive research, and seek professional advice
before engaging in live trading activities. This disclaimer is not exhaustive, and users should consider all relevant
factors before making trading decisions.

By using or accessing any software libraries and modules therein, users acknowledge and accept the risks outlined
in this disclaimer. `LucidInvestor <https://lucidinvestor.ca/>`_ and other providers of algorithms, and open source
software such as `backtrader and backtrader-contrib <https://gitlab.com/algorithmic-trading-library>`_, shall not be
held liable for any losses incurred as a result of live trading activities.

IBAPI 3rd party library is a requirement
=========================================
Interaction with this implementation of IB is done by using the Native IBAPI library and this has to be installed
prior to usage. Although an outdated (still working) package is available in Pypi, the up-to-date stable version of
the IBAPI should be downloaded from: `https://interactivebrokers.github.io/ <https://interactivebrokers.github.io/>`_.

Follow `instruction <https://ibkrcampus.com/ibkr-api-page/trader-workstation-api/>`_ to get IBAPI up and running
in your environment. Release notes for IBAPI are available `here <https://ibkrguides.com/releasenotes/tws-api.htm>`_.

Credits
========
This work is based on the original work of Atreyu Group LLC, released under BSD 2-Clause License, and available at
`github.com/atreyuxtrading/atreyu-backtrader-api <https://github.com/atreyuxtrading/atreyu-backtrader-api>`_.


