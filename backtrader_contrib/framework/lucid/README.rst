LucidInvestor
=============

`LUCID <https://lucidinvestor.ca/>`_ is a social platform that helps individual investors who want to control their
financial lives, to collectively analyze the real life results of investment strategies, and turn self-managed
portfolios into personalized automated trading systems.

With an `Open Core based on Backtrader <https://gitlab.com/algorithmic-trading-library>`_, anyone can
review public strategies source code, backtest and run them live on either `LUCID <https://lucidinvestor.ca/>`_ or
a self-managed Backtrader solutions.

    note: a self-managed Backtrader solutions means that it's one's responsibility to code all software necessary
    to manage and orchestrate all aspects of trading orders flow and execution on top of Backtrader.

Portfolio Strategies
---------------------

Implementations of Limited Model Portfolio ("LMP") are available in the sub-module
`backtrader_contrib/strategies/lucid </backtrader_contrib/strategies/lucid>`__.

The LucidInvestor Platform directly implements these open source strategies, implementing the necessary methods
overloading to manage and orchestrate all aspects of live trading orders flow and execution on top of Backtrader.

*LMP* are a subset of model portfolios that are well-researched, broadly published and easily accessible to investors;
LMP have the particularity to be limited to class of investors (not tailored to an individual), class of assets, and
industry sectors, without referencing specific securities or issuers.

Usage
-----

As simple as:

.. code-block:: python

  import backtrader_contrib as bt
  from backtrader_contrib.framework.lucid.strategy_generic import StrategyGeneric

  class MyStrategy(StrategyGeneric):
  ....

Example
********

Refer to
`fixed_target_allocation.py </backtrader_contrib/strategies/lucid/strategic_asset_allocation/fixed_target_allocations.py>`__.

.. image:: /backtrader_contrib/strategies/lucid/strategic_asset_allocation/60-40.png
   :width: 400
   :alt: 60-40 portfolio backtest on daily data.

Getting involved
----------------

contact us at info@lucidinvestor.ca

Open source licensing info
--------------------------
`BSD 3-Clause <LICENSE.bsd3>`__

.. include:: LICENSE.bsd3

DISCLAIMER: RISKS ASSOCIATED WITH LIVE TRADING ALGORITHMS
=========================================================

The deployment of algorithms to live trading involves inherent risks that may result in the loss of capital.
Users are advised to carefully consider and understand the various risks associated with live trading, including but
not limited to strategy risks, portfolio risks, market risks, counterparty risks, operational risks, and error risks.

Strategy Risks:
The effectiveness of trading strategies is subject to market conditions, and unforeseen events may impact their
performance. Users should be aware that strategies may not always yield the desired results, and past performance
is not indicative of future success.

Portfolio Risks:
Diversification strategies may not fully protect against losses, and the performance of an entire portfolio may be
affected by the performance of individual assets within it.

Market Risks:
Fluctuations in market conditions and unexpected events can lead to significant financial losses. Users should be
prepared for market volatility and the potential impact on their trading strategies.

Counterparty Risks:
Transactions involve counterparties, and the failure of a counterparty can lead to financial losses. Users should
conduct due diligence on counterparties and be aware of the associated risks.

Operational Risks:
Operational risks pertain to business operations, including regulatory changes, business risks, and trading
infrastructure risks. Users are encouraged to stay informed about regulatory changes, utilize open-source trading
infrastructure maintained by experts, and consider co-located servers to mitigate hardware failures and internet
outages.

Error Risks:
Error risks are associated with errors in strategy logic or trading infrastructure. Bugs in trading algorithms and
underlying engines may lead to financial losses. Users should conduct thorough backtesting to identify coding errors
before deploying algorithms live and stay informed about relevant software issues.

While efforts can be made to mitigate these risks, it is important to note that some risks may be beyond the user's
control. Users are strongly advised to exercise caution, conduct comprehensive research, and seek professional advice
before engaging in live trading activities. This disclaimer is not exhaustive, and users should consider all relevant
factors before making trading decisions.

By using or accessing any software libraries and modules therein, users acknowledge and accept the risks outlined
in this disclaimer. `LucidInvestor <https://lucidinvestor.ca/>`_ and other providers of algorithms, and open source
software such as `backtrader and backtrader-contrib <https://gitlab.com/algorithmic-trading-library>`_, shall not be
held liable for any losses incurred as a result of live trading activities.